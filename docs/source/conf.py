
project = 'Ali Zinsaz'
copyright = '2023, Ali Zinsaz'
author = 'Ali'

source_suffix = ['.rst', '.md']
extensions = ["myst_parser",
              "sphinx_design"]
myst_enable_extensions = ["colon_fence"]


templates_path = ['_templates']
exclude_patterns = []


html_show_sphinx = False
html_theme = "furo"
html_static_path = ['_static', '_static/illustrations', '_static/icons']
html_title = "Ali Zinsaz"
html_theme_options = {
    # "light_logo": "images/brand/light_logo.svg",
    # "dark_logo": "images/brand/dark_logo.svg",
    "light_css_variables": {
        'color-background-primary': '#F4F4F4',
        'color-background-secondary': '#EEEEEE',
        'color-link': '#505050',
        'color-link--hover': '#CA55F3'},
    "dark_css_variables": {
        'color-background-primary': '#060915',
        'color-background-secondary': '#090659',
        'color-link': '#FFFFFF',
        'color-link--hover': '#CA55F3'},
    "navigation_with_keys": True
}
