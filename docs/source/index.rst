.. image:: ./_static/illustrations/index.png
   :align: center

Welcome to my personal website! It's a pleasure to have you here. Take a moment to explore and discover the intersection
of my passion for physics and love for computer experimentation. From developing cutting-edge trading algorithms to
exploring the depths of scientific curiosity, this space embodies the essence of my journey. Feel free to delve into my
projects and accomplishments, and don't hesitate to reach out for collaborations or inquiries. Thank you for visiting,
and I hope this digital rendezvous leaves you inspired and intrigued.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   pages/who_am_i.md
   pages/what_i_do.md
   pages/skills.md
   pages/personal_projects.md
   pages/hobbies.md
   pages/contact.md

.. image:: ./_static/icons/index_footer.svg
   :align: center
