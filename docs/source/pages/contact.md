```{image} /_static/illustrations/contact.svg
:align: center
```

# Contact

- <img src="../_static/icons/email.svg" alt="Icon" style="width:1.2em; height:1.2em;"> Email [azinsaz@gmail.com](mailto:azinsaz@gmail.com)

- <img src="../_static/icons/linkedin.svg" alt="Icon" style="width:1.2em; height:1.2em;"> LinkedIn [Ali Zinsaz](https://www.linkedin.com/in/ali-zinsaz/)

- {octicon}`git-branch;1.2em` GitLab [azinsaz](https://www.linkedin.com/in/ali-zinsaz/)

- <img src="../_static/icons/github.svg" alt="Icon" style="width:1.2em; height:1.2em;"> GitHub [azinsaz](https://github.com/azinsaz)

- <img src="../_static/icons/twitter.svg" alt="Icon" style="width:1.2em; height:1.2em;"> Twitter [@azinsaz](https://twitter.com/azinsaz?t=HCg72Osq73_dzTKVeHq8mw&s=09)

- <img src="../_static/icons/facebook.svg" alt="Icon" style="width:1.2em; height:1.2em;"> facebook [Ali Zinsaz](https://www.facebook.com/ali.zinsaz.5?mibextid=ZbWKwL)

- <img src="../_static/icons/instagram.svg" alt="Icon" style="width:1.2em; height:1.2em;"> instagram [@ali_zinsaz](https://instagram.com/ali_zinsaz?igshid=NGExMmI2YTkyZg==)
