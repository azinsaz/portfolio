```{image} /_static/illustrations/skills_illus.svg
:align: center
```

# Skills

My professional skills are visualized through a schematic representation, showcasing the breadth of my expertise.

```{image} /_static/illustrations/skills.png
:align: center
```

My expertise revolves around three key areas, illustrated in the overlapping sections of a Venn diagram. These areas form 
the foundation of my professional skill set and contribute to my success in various projects and endeavors.

```{image} /_static/illustrations/venn.png
:align: center
```
