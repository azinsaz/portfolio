```{image} /_static/illustrations/whatIDo.svg
:align: center
```

# What I do?

```{image} /_static/icons/wifi.svg
:align: center
```

In my role as an automation developer at Spark Microsystems in the silicon industry, I have the privilege of working on 
cutting-edge technology in the field of telecommunications. Specifically, our focus is on developing advanced 
telecommunications chips that leverage the incredible capabilities of Ultra Wide Band (UWB) technology. UWB allows for 
high-speed data transfer with remarkably low power consumption and latency.

```{image} /_static/icons/wafer.jpg
:align: center
```

On a daily basis, I engage in a diverse range of tasks and projects that span both the hardware and software sides of development. 
One of my primary responsibilities is creating automated test benches and optimizing traditional workflows to enhance efficiency. 
Additionally, I play a key role in automated validation and quality assurance processes, implementing DevOps practices 
for seamless integration and continuous delivery.

```{image} /_static/icons/devops.svg
:align: center
```

My programming language of choice is Python, which I extensively utilize in my work. I am also well-versed in implementing
DevOps practices using GitLab CI/CD, ensuring smooth collaboration and deployment across the development cycle. 
The projects I engage in involve a combination of hardware (chips, carrier boards), firmware (embedded system software), 
software (for validation and automation), as well as data storage and analysis.

```{image} /_static/icons/checklist.svg
:align: center
```

Furthermore, I am no stranger to working on application-level projects, ranging from audio and mouse technologies to VR 
goggles and data transfer. Sometimes, I even get the opportunity to explore minimal design and utilize 3D printing 
techniques to bring ideas to life.

```{image} /_static/icons/vr_goggles.svg
:align: center
```

By embracing the challenges and possibilities of this multidisciplinary work, I thrive in bringing automation projects 
from conception to completion. With a strong focus on efficiency, reliability, and innovation, I continuously strive to 
push the boundaries of what is achievable in the dynamic world of telecommunications and beyond.

```{image} /_static/icons/state.svg
:align: center
```
