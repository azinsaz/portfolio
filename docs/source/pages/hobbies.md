```{image} /_static/illustrations/hobbies.svg
:align: center
```

# Hobbies

When I'm not immersed in my professional pursuits, you'll find me indulging in a range of exciting hobbies that reflect 
my thirst for knowledge, adventure, and exploration. Learning is a constant passion of mine, especially when it comes to 
diving into the captivating worlds of physics and philosophy. I love unraveling the mysteries of the universe and delving 
into profound ideas that expand my intellectual horizons.

```{image} /_static/icons/science.svg
:align: center
```

But it's not all about the mind. My adventurous spirit craves thrilling experiences and adrenaline-pumping activities. 
I'm an avid traveler, always seeking new destinations that offer a tapestry of culture, breathtaking landscapes, and 
unforgettable memories. Whether I'm exploring ancient ruins or trekking through lush mountains, I cherish every 
opportunity to broaden my perspective and connect with the world around me.

```{image} /_static/icons/backpack.svg
:align: center
```

When I'm not globe-trotting, you'll often find me engaging in climbing, biking, skiing, or camping. 
Nature is my playground, and I relish the freedom and serenity it offers.

```{image} /_static/icons/tent.svg
:align: center
```

And let's not forget volleyball, a sport that 
fuels my competitive spirit and brings out my team-player mentality. The rush of the game, the camaraderie with fellow 
players, and the joy of a well-executed spike—it's a passion that keeps me active and energized.

```{image} /_static/icons/volleyball.svg
:align: center
```
