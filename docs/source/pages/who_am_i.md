```{image} /_static/illustrations/whoAmI.svg
:align: center
```

# Who Am I?

```{image} /_static/icons/baby.svg
:align: center
```

Born in Tehran, Iran in 1993, my passion for science was evident from an early age. Throughout high school, I developed 
a keen interest in computers, but it was my fascination with physics that truly blossomed during this time. As I prepared 
for physics olympiad competitions, I had the privilege of learning from some of the nation's top educators, which deepened 
my appreciation for the wonders of physics.

```{image} /_static/icons/book.svg
:align: center
```

Fast forward to my university days at the University of Tehran, where I pursued my engineering studies. Alongside my 
academic journey, I discovered the joy of teaching physics to high school students gearing up for olympiads. It was a 
magical experience that deepened my understanding and ignited an even greater fire within me.

```{image} /_static/icons/engineering.svg
:align: center
```

But life had more in store for me. 
After completing my engineering degree, I made the decision to delve deeper into 
physics academically. I embarked on a Bachelor's program in Physics at Concordia University in Montréal, Canada, 
graduating in 2021. Since then, I've been diving into the thrilling world of the silicon (transistors) industry, where science and 
technology collide.

```{image} /_static/icons/idea.svg
:align: center
```

During the day, I immerse myself into exciting cutting-edge projects that span both hardware and software realms. 
It's a thrilling journey where I get to explore the latest innovations and push the boundaries of what's possible. 
And when I have those precious moments of free time, I indulge my insatiable curiosity in a variety of ways. Whether 
it's diving into personal projects, pondering the mysteries of philosophy, unraveling the secrets of physics, or 
embracing the great outdoors and of course some volleyball fun, I'm always seeking new adventures to fuel my hunger for 
knowledge and experience.

```{image} /_static/icons/night.svg
:align: center
```

This journey has allowed me to blend my passion for science, computers, and physics, creating a unique perspective that 
drives my ongoing professional and personal pursuits.

```{image} /_static/icons/growth.svg
:align: center
```
