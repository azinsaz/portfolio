```{image} /_static/illustrations/personalProjects.svg
:align: center
```

# Personal Projects

I absolutely love filling my free time with fun and engaging personal projects that allow me to learn and grow. 
It's through these hands-on experiences that I truly thrive and find the most enjoyment. By combining the elements of 
fun and learning, I create an environment where I can maximize the excitement while acquiring new knowledge and skills. 
Whether it's diving into innovative applications or exploring the latest technologies, I relish the opportunity to 
embark on fulfilling adventures that expand my horizons. These projects not only bring a sense of joy and fulfillment 
but also contribute to my personal and professional development.

## Eigen FinTech

```{image} /_static/icons/eigenFinTech.svg
:align: center
```

In close collaboration with a team of two friends, we embarked on an exciting journey to develop an algorithmic trading 
platform that seamlessly integrates machine learning with traditional trading strategies. Our aim was to automate the 
entire trading process through computer programming, harnessing the power of technology to enhance decision-making and 
maximize profitability.

As we delved deeper into our project, we realized the immense potential for further expansion. We eagerly explored the 
integration of different layers of machine learning techniques, leveraging them for various purposes. From trend and 
price forecasting to portfolio optimization, we implemented cutting-edge algorithms to enhance the performance and 
accuracy of our platform.

Beyond just automation and machine learning, we recognized the importance of providing valuable insights and tools to our 
users. Our platform goes beyond mere execution by offering comprehensive financial market analysis and real-time testing 
capabilities. Traders can explore and test their strategies through backtesting, empowering them to make informed 
decisions based on historical data and market trends.

Engaging in this project has been a transformative learning experience for me, encompassing a wide range of development 
concepts and technologies. From backend development to data analysis, frontend development, DevOps practices, Docker, 
and Git, I immersed myself in various aspects of the development process.

## Robotic Guitar Player

```{image} /_static/icons/guitar.svg
:align: center
```

Recently, I stumbled upon the remarkable capabilities of the Rust programming language, and it immediately captured my curiosity. 
As someone who thrives on hands-on learning experiences, I made the decision to start a personal project, immersing 
myself deeper into the Rust world.

My project centers around the creation of a robotic device that brings melodies to life by playing them on a real guitar. 
This endeavor allows me to explore a diverse range of domains, including both software and hardware realms. 
The amalgamation of these elements presents an exciting opportunity to delve into multiple disciplines simultaneously.

Currently, the project is in its research and development phase. With every step, I am not only expanding my knowledge 
but also fostering a deep understanding of the Rust language and its applications. Learning remains the primary focus 
as I strive to absorb as much as possible from this hands-on exploration.

By undertaking this project, I am not only honing my skills in Rust but also gaining invaluable experience in areas such
as mechanical design, hardware integration, and embedded systems. The interdisciplinary nature of this endeavor fuels my 
passion for learning and challenges me to think creatively and innovatively.

## AstroPhysical Simulations

```{image} /_static/icons/astroPhysics.svg
:align: center
```

Cosmology holds a special place in my heart as my greatest passion in life. In my pursuit of a deeper understanding of 
the universe, I seize every opportunity to merge my two beloved subjects: computers and physics. Through the realm of 
computational physics, I embark on captivating journeys that unravel the mysteries of our cosmos.

Using programming languages like Java and Python, I have harnessed the power of simulation to delve into awe-inspiring 
phenomena such as black holes, planetary systems, and the mesmerizing intricacies of fractals. These simulations provide 
me with profound insights into the building blocks and dynamics that shape our vast universe.
